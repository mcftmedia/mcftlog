package net.diamondmine.mcftlog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Core of logger.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class McftLogger {
    public static int limit = 10000000;
    public static int numLogFiles = 1000;
    public static Formatter formatter = new Formatter() {
        @Override
        public String format(final LogRecord rec) {
            StringBuffer buf = new StringBuffer(1000);
            buf.append(calcDate(rec.getMillis()));
            buf.append(' ');
            buf.append(formatMessage(rec));
            buf.append('\n');
            return buf.toString();
        }
    };

    public static Logger commlog;
    static {
        try {
            FileHandler fh = new FileHandler("plugins/McftLog/CommandLog/CommandLog.log", limit, numLogFiles, true);
            fh.setFormatter(formatter);
            commlog = Logger.getLogger("Minecraft.McftLog.Command");
            commlog.addHandler(fh);
            commlog.setUseParentHandlers(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Logger chatlog;
    static {
        try {
            FileHandler fh = new FileHandler("plugins/McftLog/ChatLog/ChatLog.log", limit, numLogFiles, true);
            fh.setFormatter(formatter);
            chatlog = Logger.getLogger("Minecraft.McftLog.Chat");
            chatlog.addHandler(fh);
            chatlog.setUseParentHandlers(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String calcDate(final long millisecs) {
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate);
    }
}