package net.diamondmine.mcftlog.listeners;

import java.util.logging.Logger;

import net.diamondmine.mcftlog.Config;
import net.diamondmine.mcftlog.McftLog;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Listeners for logging.
 * 
 * @author Jon la Cour
 * @version 1.0.3
 */
public class PlayerListener implements Listener {
    private final Config config;
    public Logger commlog;
    public Logger chatlog;

    public PlayerListener(final McftLog instance) {
        config = instance.conf;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent event) {
        if (event.isCancelled()) {
            return;
        }
        String message = event.getMessage();

        int i = message.indexOf(' ');
        if (i < 0) {
            i = message.length() - 1;
        }

        // if (config.isToChat((String) message.subSequence(0, i))) {
        // chatlog.info(event.getPlayer().getName() + ": " + message);
        // }

        if (config.isNoCommand((String) message.subSequence(0, i))) {
            return;
        } else {
            commlog.info(event.getPlayer().getName() + ": " + message);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerChat(final AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;
        }
        chatlog.info(event.getPlayer().getName() + ": " + event.getMessage());
    }
}
