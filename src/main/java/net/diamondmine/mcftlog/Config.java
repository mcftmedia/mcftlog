package net.diamondmine.mcftlog;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.configuration.Configuration;

/**
 * Configuration handler.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class Config {
    private final McftLog plugin;
    private final Configuration conf;
    private List<String> toChat = new LinkedList<String>();
    private List<String> noCommand = new LinkedList<String>();

    public Config(final McftLog instance) {
        plugin = instance;
        conf = instance.getConfig();
    }

    private void load() {
        plugin.reloadConfig();
        toChat();
        noCommand();
    }

    public void reload() {
        load();
    }

    private void toChat() {
        toChat.clear();
        toChat = conf.getStringList("toChat.");
    }

    private void noCommand() {
        noCommand.clear();
        noCommand = conf.getStringList("noCommand.");
    }

    public boolean isToChat(final String message) {
        if (toChat.contains(message)) {
            return true;
        }
        return false;
    }

    public boolean isNoCommand(final String message) {
        if (noCommand.contains(message)) {
            return true;
        }
        return false;
    }
}