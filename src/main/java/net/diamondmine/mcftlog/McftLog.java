package net.diamondmine.mcftlog;

import java.io.File;
import java.util.logging.Logger;

import net.diamondmine.mcftlog.listeners.PlayerListener;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftLog.
 * 
 * @author Jon la Cour
 * @version 1.0.2
 */
public class McftLog extends JavaPlugin {
    private final PlayerListener playerListener = new PlayerListener(this);
    private final Logger logger = Logger.getLogger("Minecraft");
    public Config conf;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public void onEnable() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(playerListener, this);

        new File("plugins/McftLog/ChatLog/").mkdirs();
        new File("plugins/McftLog/CommandLog/").mkdirs();

        log("Version " + getDescription().getVersion() + " enabled");

        conf = new Config(this);
        conf.reload();

        playerListener.commlog = McftLogger.commlog;
        playerListener.chatlog = McftLogger.chatlog;
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.1
     */
    public void log(final String s, final String type) {
        String message = "[McftLog] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public void log(final String s) {
        log(s, "info");
    }
}