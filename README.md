# McftLog

A Bukkit server plugin for Minecraft that logs chat and commands in two separate files cleanly, as well as logging all player commands into the console.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=LOG)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/LOG-LOG)**
-- **[Download](http://diamondmine.net/plugins/download/McftLog)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftlog/)**